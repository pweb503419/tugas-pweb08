function validateUsername(username) {
    if (!username) {
        return "Username is required";
    } else if (username.length < 3) {
        return "Username must be at least 3 characters long";
    }
    return "";
}

function validateEmail(email) {
    const emailPattern = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    if (!email) {
        return "Email is required";
    } else if (!emailPattern.test(email)) {
        return "Invalid email format";
    }
    return "";
}

function validatePassword(password) {
    if (!password) {
        return "Password is required";
    } else if (password.length < 6) {
        return "Password must be at least 6 characters long";
    }
    return "";
}

function validateConfirmPassword(password, confirmPassword) {
    if (!confirmPassword) {
        return "Confirm password is required";
    } else if (password !== confirmPassword) {
        return "Passwords do not match";
    }
    return "";
}

function validatePhone(phone) {
    const phonePattern = /^\d{10,12}$/;
    if (!phone) {
        return "Phone number is required";
    } else if (!phonePattern.test(phone)) {
        return "Phone number must be 10-12 digits long";
    }
    return "";
}

function validateDob(dob) {
    if (!dob) {
        return "Date of birth is required";
    }

    const age = getAge(new Date(dob));
    if (age < 18) {
        return "Age must be at least 18";
    }

    return "";
}

function getAge(dob) {
    const diff = Date.now() - dob.getTime();
    const ageDate = new Date(diff);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}

function runProgram() {
    let age;
    let isValid = false;

    do {
        age = prompt("Masukkan usia Anda:");
        isValid = age >= 18;
        if (!isValid) {
            alert("Usia harus minimal 18 tahun");
        }
    } while (!isValid);
}

function validateForm() {
    let isValid = true;

    const username = document.getElementById('username').value;
    const email = document.getElementById('email').value;
    const password = document.getElementById('password').value;
    const confirmPassword = document.getElementById('confirmPassword').value;
    const phone = document.getElementById('phone').value;
    const dob = document.getElementById('dob').value;

    const usernameError = document.getElementById('usernameError');
    const emailError = document.getElementById('emailError');
    const passwordError = document.getElementById('passwordError');
    const confirmPasswordError = document.getElementById('confirmPasswordError');
    const phoneError = document.getElementById('phoneError');
    const dobError = document.getElementById('dobError');

    usernameError.textContent = validateUsername(username);
    emailError.textContent = validateEmail(email);
    passwordError.textContent = validatePassword(password);
    confirmPasswordError.textContent = validateConfirmPassword(password, confirmPassword);
    phoneError.textContent = validatePhone(phone);
    dobError.textContent = validateDob(dob);

    if (usernameError.textContent) {
        document.getElementById('username').classList.add('error-border');
        isValid = false;
    } else {
        document.getElementById('username').classList.remove('error-border');
    }

    if (emailError.textContent) {
        document.getElementById('email').classList.add('error-border');
        isValid = false;
    } else {
        document.getElementById('email').classList.remove('error-border');
    }

    if (passwordError.textContent) {
        document.getElementById('password').classList.add('error-border');
        isValid = false;
    } else {
        document.getElementById('password').classList.remove('error-border');
    }

    if (confirmPasswordError.textContent) {
        document.getElementById('confirmPassword').classList.add('error-border');
        isValid = false;
    } else {
        document.getElementById('confirmPassword').classList.remove('error-border');
    }

    if (phoneError.textContent) {
        document.getElementById('phone').classList.add('error-border');
        isValid = false;
    } else {
        document.getElementById('phone').classList.remove('error-border');
    }

    if (dobError.textContent) {
        document.getElementById('dob').classList.add('error-border');
        isValid = false;
    } else {
        document.getElementById('dob').classList.remove('error-border');
    }

    if (isValid) {
        alert("Form berhasil dikirim!");
    }

    return isValid;
}
